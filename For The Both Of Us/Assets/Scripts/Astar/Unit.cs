﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

	public float stopingDistance;
	public Transform target;
	public float speed = 20;
	[SerializeField] Vector2 offsetCenter;
	[SerializeField] Vector2 sizeInNodes;
	Rigidbody2D rb;
	Animator anim;
	SpriteRenderer spr;
	Vector2 lastPosition;

	List<Node> nodesIOcupy;
	List<Node> NodesIOcupy{
		get{
			return nodesIOcupy;
		}
		set{
			nodesIOcupy = value;
		}
	}

	Vector2[] path;
	int targetIndex;

	Vector2 walkDirection{
		get{
			if(playerDistance > stopingDistance){
				Vector2 dir = currentWaypoint - ((Vector2)transform.position + offsetCenter);
				return dir.normalized;
			}else{
				return Vector2.zero;
			}
		}
	}

	float playerDistance{
		get{return Vector2.Distance (transform.position, target.position);}
	}

	bool playingAttackAnimation{
		get{return anim.GetCurrentAnimatorStateInfo (0).IsName ("Base.Attack");}
	}

	bool isMoving{
		get{if(walkDirection != Vector2.zero) return true; else return false;}
	}

	Vector2 currentWaypoint;

	void Start() {
		NodesIOcupy = new List<Node> ();
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		spr = GetComponent<SpriteRenderer> ();
		StartCoroutine (RefreshPath ());
	}

	void Update(){
		UpdateNodes ();
	}

	void UpdateNodes(){
		if ((Vector2)transform.position == lastPosition) return;

		if (sizeInNodes == Vector2.zero) {
			Node node = Pathfinding.instance.grid.NodeFromWorldPoint (transform.position);
			node.occupied = true;
			lastPosition = transform.position;
		} else {
			Node node = Pathfinding.instance.grid.NodeFromWorldPoint (transform.position);
			foreach (var nodes in Pathfinding.instance.grid.GetNeighbours(node,(int)sizeInNodes.x,(int)sizeInNodes.y)) {
				nodes.occupied = true;	
			}
			lastPosition = transform.position;
		}
	}

	IEnumerator RefreshPath() {
		Vector2 targetPositionOld = (Vector2)target.position + Vector2.up; // ensure != to target.position initially

		while (true) {
			if (targetPositionOld != (Vector2)target.position) {
				targetPositionOld = (Vector2)target.position;

				path = Pathfinding.RequestPath ((Vector2)transform.position + offsetCenter, target.position);
				StopCoroutine ("FollowPath");
				StartCoroutine ("FollowPath");
			}

			yield return new WaitForSeconds (.25f);
		}
	}

	IEnumerator FollowPath() {
		if (path.Length > 0) {
			targetIndex = 0;
			currentWaypoint = path [0];

			while (true) {
				if (Vector2.Distance((Vector2)transform.position + offsetCenter,currentWaypoint) < stopingDistance) {
					targetIndex++;
					if (targetIndex >= path.Length) {
						yield break;
					}
					currentWaypoint = path [targetIndex];
				}

				Movement ();
				yield return null;

			}
		}
	}

	void Movement(){
		if (!playingAttackAnimation) {
			rb.velocity = walkDirection * speed;
		}
		anim.SetBool ("isMoving", isMoving);
		if (walkDirection.x > 0.1) {
			spr.flipX = false;
		} else if (walkDirection.x < -0.1) {
			spr.flipX = true;
		}
	}

	public void OnDrawGizmos() {
		if (path != null) {
			for (int i = targetIndex; i < path.Length; i ++) {
				Gizmos.color = Color.black;
				Gizmos.DrawCube((Vector3)path[i], Vector3.one *.2f);

				if (i == targetIndex) {
					Gizmos.DrawLine((Vector2)transform.position + offsetCenter, path[i]);
				}
				else {
					Gizmos.DrawLine(path[i-1],path[i]);
				}
			}
		}
	}
}