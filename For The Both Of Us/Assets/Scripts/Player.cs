﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	Rigidbody2D rb;
	Animator anim;
	[SerializeField] LayerMask enemyLayer;


	public float attackRadius;
	public float speed;

	Vector2 playerInput{
		get{return new Vector2 (Input.GetAxis("Horizontal"),Input.GetAxis("Vertical")).normalized;}
	}

	bool playingAttackAnimation{
		get{return anim.GetCurrentAnimatorStateInfo (0).IsName ("Base.Attack");}
	}

	bool isMoving{
		get{if(playerInput != Vector2.zero) return true; else return false;}
	}

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}
	
	void Movement(){
		if (!playingAttackAnimation) {
			rb.velocity = playerInput * speed;
		}
		anim.SetBool ("isMoving", isMoving);
		if (playerInput.x > 0.1) {
			transform.localScale = new Vector3 (1, 1, 1);
		} else if (playerInput.x < -0.1) {
			transform.localScale = new Vector3 (-1, 1, 1);
		}
	}

	void LookForAttack(){
		if (Input.GetKeyDown (KeyCode.Z) && !playingAttackAnimation) {
			anim.SetTrigger ("attack");
			rb.velocity = Vector2.zero;
		}
	}

	void Update () {
		Movement ();
		LookForAttack ();
	}

	void Attack(){
		Collider2D[] enemiesInRadius = Physics2D.OverlapCircleAll (transform.position, attackRadius, enemyLayer);
		if (enemiesInRadius.Length == 0) return;
		foreach (var co in enemiesInRadius) {
			Destroy (co.gameObject);
		}
	}
}
