﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	Rigidbody2D rb;
	Animator anim;
	SpriteRenderer spr;

	Transform player;
	[SerializeField] LayerMask enemyLayer;

	public float attackRadius;
	public float stopingDistance;
	public float speed;
	public float attackInterval;
	float currentCooldown;

	float playerDistance{
		get{return Vector2.Distance (transform.position, player.position);}
	}

	Vector2 walkDirection{
		get{
			if (playerDistance >= stopingDistance) {
				Vector2 dir = player.position - transform.position;
				return dir.normalized;
			} else {
				if (Time.time > currentCooldown) {
					anim.SetTrigger ("attack");
					rb.velocity = Vector2.zero;
					currentCooldown = Time.time + attackInterval;
				}
				return Vector2.zero;
			}
		}
	}

	bool playingAttackAnimation{
		get{return anim.GetCurrentAnimatorStateInfo (0).IsName ("Base.Attack");}
	}

	bool isMoving{
		get{if(walkDirection != Vector2.zero) return true; else return false;}
	}

	void Start () {
		player = FindObjectOfType<Player> ().transform;
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		spr = GetComponent<SpriteRenderer> ();
	}

	void Movement(){
		if (!playingAttackAnimation) {
			rb.velocity = walkDirection * speed;
		}
		anim.SetBool ("isMoving", isMoving);
		if (walkDirection.x > 0.1) {
			spr.flipX = false;
		} else if (walkDirection.x < -0.1) {
			spr.flipX = true;
		}
	}

	void Update () {
		Movement ();
	}

	void Attack(){
		Collider2D[] enemiesInRadius = Physics2D.OverlapCircleAll (transform.position, attackRadius, enemyLayer);
		if (enemiesInRadius.Length == 0) return;
		foreach (var co in enemiesInRadius) {
			//Destroy (co.gameObject);
			Debug.Log("Hit ya");
		}
	}
}
